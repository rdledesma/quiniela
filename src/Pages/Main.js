import React, { useState, Fragment, useEffect } from 'react';


export default function Main() {
  const data = [
    {
      id: '1',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
        {
          tipo: 'REDOBLONA', num: '40', to: '10', num2: '10', to2: '10', amount: '200',
        },
        {
          tipo: 'REDOBLONA', num: '1', to: '1', num2: '11', to2: '10', amount: '10',
        },
      ],
      horario: 'HORARIO 1',
      quiniela: ['Q1', 'Q2', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '2',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 2',
      quiniela: ['Q1', 'Q2', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '3',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '4',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '5',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '6',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '7',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '8',
      jugador: 'Dario',
      apuestas: [
        {
          tipo: 'QUINIELA', num: '40', to: '10', amount: '200',
        },
        {
          tipo: 'QUINIELA', num: '1', to: '1', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
    {
      id: '9',
      jugador: 'Leo',
      apuestas: [
        {
          tipo: 'REDOBLONA', num: '40', to: '10', num2: '10', to2: '10', amount: '200',
        },
        {
          tipo: 'REDOBLONA', num: '1', to: '1', num2: '11', to2: '10', amount: '10',
        },
      ],
      horario: 'HORARIO 3',
      quiniela: ['Q1', 'Q3'],
      fecha: '2020-06-01',
    },
  ];

  const [name, setName] = useState('');

  const filter = (e) => {
    setName(e.target.value);
    datos = data.filter((el) => el.jugador.indexOf(name) > -1);
  };


  let datos = data;


  const listado = () => (
    <tbody>
      {datos.filter((el) => (el.jugador.indexOf(name) > -1)).map((item, i) => (
        <>
          <tr key={i} style={{ backgroundColor: '#4DD13D' }}>
            <td>{item.jugador}</td>
            <td>{item.fecha}</td>
            <td>{item.horario}</td>
            <td>{item.quiniela}</td>
          </tr>
          <tr style={{ backgroundColor: ' #6D98D1' }}>
            <th scope="col">Nro</th>
            <th scope="col">Hasta</th>
            <th scope="col">Nro(2)</th>
            <th scope="col">Hasta(2)</th>
            <th scope="col">Monto</th>
          </tr>
          {item.apuestas.map((elemento) => (


            elemento.tipo === 'QUINIELA'
              ? (
                <tr style={{ backgroundColor: ' #6D98D1' }}>
                  <td>{elemento.num}</td>
                  <td>{elemento.to}</td>
                  <td />
                  <td />
                  <td>{elemento.amount}</td>
                </tr>
              ) : (
                <tr style={{ backgroundColor: ' #6D98D1' }}>
                  <td>{elemento.num}</td>
                  <td>{elemento.to}</td>
                  <td>{elemento.to2}</td>
                  <td>{elemento.num2}</td>
                  <td>{elemento.amount}</td>
                </tr>
              )

          ))}
        </>
      ))}

    </tbody>
  );


  useEffect(() => {
    listado();
    datos = data.filter((el) => el.jugador.indexOf(name) > -1);
    
  }, [name, datos]);


  return (
    <div>
      <h3>Listado de apuestas</h3>
      <div className="row">
        <div className="col-6">
          <div className="form-group">
            <input onChange={(e) => filter(e)} value={name} type="name" className="form-control" placeholder="Nombre" />
          </div>
        </div>
      </div>

      <div className="table-responsive">
        <table className="table table-hover table-sm" cellSpacing="0" cellPadding="0">
          <thead style={{ backgroundColor: '#339E67' }}>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Fecha</th>
              <th scope="col">Horario</th>
              <th scope="col">Quinielas</th>
            </tr>
          </thead>
          {listado()}
        </table>
      </div>
    </div>
  );
}
