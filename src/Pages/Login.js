/* eslint-disable react/prop-types */
import React from 'react';

export default function Login(props) {
  const { user, setUser, handleSubmit } = props;

  const handleSubmit2 = (e) => {
    e.preventDefault();
    handleSubmit();
  };


  return (
    <div className="row text-center justify-content-center mb-5">
      <div className="col text-center">
        <div className="card mx-auto card-register" style={{ border: 'none' }}>
          <div className="card-body">
            <br />
            <br />
            <br />
            <h3 className="h3-tally mb-2">Inicio de sesión</h3>
            <div className="row justify-content-center">
              <div className="col-11">
                <form>
                  <div className="form-group">
                    <input className="form-control" onChange={(e) => setUser({ ...user, email: e.target.value })} type="email" name="email" placeholder="Email" value={user.email} required />
                  </div>
                  <div className="form-group">
                    <input onChange={(e) => setUser({ ...user, password: e.target.value })} type="password" autoComplete="new-password" className="form-control" id="password" name="password" placeholder="Contraseña" value={user.password} required />

                  </div>
                  <div className="form-group">
                    <button className="btn btn-primary btn-block" type="submit" onClick={(e) => handleSubmit2(e)}>Iniciar Sesión</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <br />
          <br />
          <br />
        </div>


      </div>
    </div>


  );
}
