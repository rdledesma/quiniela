import { openDB } from 'idb';

export const QUINIELAS = ['Q1', 'Q2', 'Q3', 'Q4', 'Q5'];
export const HORARIOS = ['H1', 'H2', 'H3'];

// demo1: Getting started
export function openDataBase() {
  openDB('db', 1, {
    upgrade(db) {
      db.createObjectStore('jugadas', { autoIncrement: true });
    },
  });
}


export async function storeJugada(jugada) {
  const db1 = await openDB('db', 1);
  db1
    .add('jugadas', { ...jugada })

    .then((result) => {
      console.log('success!', result);
    })
    .catch((err) => {
      console.error('error: ', err);
    });
  db1.close();
}
