/* eslint-disable radix */
/* eslint-disable react/prop-types */
import React from 'react';
import Grid from '@material-ui/core/Grid';

export default function CustomButton(props) {
  const {
    icon, fn, title, estilos,
  } = props;

  return (
    <Grid className={estilos} onClick={fn}>
      <img src={icon} alt="desc" />
      <p>{title}</p>
    </Grid>
  );
}
