/* eslint-disable radix */
/* eslint-disable react/prop-types */
import React from 'react';
import Grid from '@material-ui/core/Grid';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

import DeleteIcon from '../Icons/borrar.png';


const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '5px 5px 5px 1px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

export default function BetForm(props) {
  const {
    id, deleteItem, jugada, setJugada, data,
  } = props;


  // const handleChange = (e) => {
  //   setApuesta({ ...apuesta, to: e.target.value });
  // };

  const onChangeNumber = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.id));
    item = { ...item, num: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.id));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };

  const onChangeTo = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.name));
    item = { ...item, to: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.name));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };


  const onChangeAmount = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.id));
    item = { ...item, amount: parseFloat(e.target.value) };
    if (e.target.value) {
      item = { ...item, amount: parseFloat(e.target.value) };
    } else {
      item = { ...item, amount: '' };
    }
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.id));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };


  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
    >
      <div className="row">
        <p>El &nbsp;&nbsp;</p>
        <BootstrapInput
          id={id}
          className="inputNumber"
          value={data.num}
          onChange={(e) => onChangeNumber(e)}
          type="number"
        />
        <Select
          name={data.id}
          style={{ marginLeft: 5, marginRight: 5 }}
          value={data.to}
          onChange={(e) => onChangeTo(e)}
          input={<BootstrapInput />}
        >
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={5}>5</MenuItem>
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={20}>20</MenuItem>

        </Select>
        <p>$</p>
        <BootstrapInput
          className="inputNumber"
          value={data.amount}
          onChange={(e) => onChangeAmount(e)}
          type="number"
          id={id}

        />
        <FormControl style={{ width: 30 }}>

          <Grid
            container
            direction="column"
            className="btnDelete"
            onClick={() => deleteItem(id)}
          >
            <img src={DeleteIcon} alt="borrar" />
            <p>BORRAR</p>
          </Grid>
        </FormControl>
      </div>

    </Grid>
  );
}
