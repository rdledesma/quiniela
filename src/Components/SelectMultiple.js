/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 250,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 5,
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}


export default function MultipleSelect(props) {
  const { items, getValue } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [personName, setPersonName] = React.useState([]);

  const handleChange = (event) => {
    setPersonName(event.target.value);
    getValue(event.target.value);
  };


  return (
    <div>
      <FormControl className={classes.formControl}>
        <Select
          multiple
          displayEmpty
          value={personName}
          onChange={handleChange}
          input={(
            <Input style={{
              borderRadius: 4,
              border: '2px solid #ced4da',
              padding: 5,
              fontSize: 20,
              textAlign: 'center',
            }}
            />
)}
          renderValue={(selected) => {
            if (selected.length === 0) {
              return <em>QUINIELAS</em>;
            }

            return selected.join(', ');
          }}
          MenuProps={MenuProps}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem disabled value="">
            <em>QUINIELAS</em>
          </MenuItem>

          {items.map((data) => (
            <MenuItem key={data} value={data} style={getStyles(data, personName, theme)}>
              {data}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {/* <FormControl className={classes.formControl}>
        <Select
          labelId="demo-mutiple-name-label"
          id="demo-mutiple-name"
          multiple
          value={personName}
          onChange={handleChange}

          input={(
            <Input style={{
              borderRadius: 4,
              border: '2px solid #ced4da',
              padding: 5,
              textAlign: 'center',
            }}
            />
)}
          MenuProps={MenuProps}
        >

          <MenuItem disabled value="">
            <em>Placeholder</em>
          </MenuItem>
          {items.map((data) => (
            <MenuItem key={data} value={data} style={getStyles(data, personName, theme)}>
              {data}
            </MenuItem>
          ))}
        </Select>
      </FormControl> */}
    </div>
  );
}
