/* eslint-disable radix */
/* eslint-disable react/prop-types */

import React from 'react';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import DeleteIcon from '../Icons/borrar.png';


const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '5px 5px 5px 1px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

export default function RedoblonaForm(props) {
  const {
    id, deleteItem, jugada, setJugada, data,
  } = props;


  const onChangeTo1 = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.name));
    item = { ...item, to1: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.name));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };

  const onChangeTo2 = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.name));
    item = { ...item, to2: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.name));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };


  const onChangeNumber1 = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.id));
    item = { ...item, num1: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.id));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };

  const onChangeNumber2 = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.id));
    item = { ...item, num2: e.target.value };
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.id));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };


  const onChangeAmount = (e) => {
    const newdata = jugada.apuestas;
    let item = newdata.find((items) => items.id === parseInt(e.target.id));
    item = { ...item, amount: parseFloat(e.target.value) };
    if (e.target.value) {
      item = { ...item, amount: parseFloat(e.target.value) };
    } else {
      item = { ...item, amount: '' };
    }
    const index = newdata.findIndex((items) => parseInt(items.id) === parseInt(e.target.id));
    newdata[index] = item;
    setJugada({ ...jugada, apuestas: newdata });
  };

  return (
    <Grid>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="flex-start"
      >
        <div className="row">
          <p>El 1</p>
          <BootstrapInput
            id={data.id}
            className="inputNumber"
            value={data.number1}
            onChange={(e) => onChangeNumber1(e)}
          />
          <Select
            style={{ marginLeft: 5, marginRight: 5 }}
            labelId="demo-simple-select-label"
            name={data.id}
            value={data.to1}
            onChange={onChangeTo1}
            input={<BootstrapInput />}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>
          </Select>
          <p className="hidden">$</p>
          <BootstrapInput
            id={data.id}
            className="inputNumber hidden "
            value={data.amount}
            onChange={(e) => onChangeAmount(e)}
            type="number"
          />
          <FormControl style={{ width: 30 }} className="hidden">

            <Grid
              container
              direction="column"
              className="btnDelete"
              onClick={() => deleteItem(id)}
            >
              <img src={DeleteIcon} alt="borrar" />
              <p>BORRAR</p>
            </Grid>
          </FormControl>
        </div>

      </Grid>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="flex-start"
      >
        <div className="row">
          <p>El 2</p>
          <BootstrapInput
            className="inputNumber"
            id={data.id}
            value={data.num2}
            onChange={(e) => onChangeNumber2(e)}
          />
          <Select
            style={{ marginLeft: 5, marginRight: 5 }}
            labelId="demo-simple-select-label"
            name={data.id}
            value={data.to2}
            onChange={onChangeTo2}
            input={<BootstrapInput />}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>

          </Select>
          <p>$</p>
          <BootstrapInput
            id={data.id}
            className="inputNumber"
            value={data.amount}
            onChange={(e) => onChangeAmount(e)}
            type="number"
          />
          <FormControl style={{ width: 30 }}>

            <Grid
              container
              direction="column"
              className="btnDelete"
              onClick={() => deleteItem(id)}
            >
              <img src={DeleteIcon} alt="borrar" />
              <p>BORRAR</p>
            </Grid>
          </FormControl>
        </div>
      </Grid>
    </Grid>
  );
}
