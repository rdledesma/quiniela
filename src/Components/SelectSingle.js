/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 250,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


export default function BetForm(props) {
  const { items, getValue } = props;
  const classes = useStyles();

  const [age, setAge] = useState('');

  const handleChange = (e) => {
    setAge(e.target.value);
    getValue(e.target.value);
  };


  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="baseline"
    >
      <FormControl className={classes.formControl}>
        <Select
          displayEmpty
          id="demo-simple-select"
          value={age}
          input={(
            <Input style={{
              borderRadius: 4,
              border: '2px solid #ced4da',
              padding: 5,
              fontSize: 20,
              textAlign: 'center',
            }}
            />
)}

          onChange={handleChange}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem disabled value="">
            <em>HORARIOS</em>
          </MenuItem>
          {items.map((data) => (
            <MenuItem value={data}>{data}</MenuItem>))}
        </Select>
      </FormControl>
    </Grid>
  );
}
