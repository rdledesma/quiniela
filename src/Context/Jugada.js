/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable import/prefer-default-export */
/* eslint-disable no-unused-vars */
import React, { useState, useMemo } from 'react';

const JugadaContext = React.createContext();

export function JugadaProvider(props) {
  const [jugada, setJugada] = useState({
    apuestas: [{
      id: 0, num: '', to: '', amount: '', type: 'QUINIELA',
    }],
    // apuestas: [{
    //   id: 0,
    //   num1: '',
    //   to1: '',
    //   num2: '',
    //   to2: '',
    //   amount: '',
    //   type: 'REDO',
    // }],
    quinielas: [],
    horario: '',
  });

  const value = useMemo(() => ({
    jugada,
    setJugada,
  }), [jugada]);


  return <JugadaContext.Provider value={value} {...props} />;
}

export function useJugada() {
  const context = React.useContext(JugadaContext);
  if (!context) {
    throw new Error('useJugada debe estar dentro del proveedor useJugada');
  }
  return context;
}
