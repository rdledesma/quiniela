/* eslint-disable array-callback-return */
import React, { useState, useEffect } from 'react';
import './App.css';
import firebase from './Config/firebase';

import LoginPage from './Pages/Login';
import Main from './Pages/Main';

export default () => (

  <App />

);

function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState('');

  const [user, setUser] = useState({
    email: '',
    password: '',
  });


  const handleSubmit = async () => {
    
    await firebase.auth().signInWithEmailAndPassword(user.email, user.password);
    verify();

  };

  const handleLogout = async () => {
    await firebase.auth().signOut();
  };

  const verify = async () => {
    setIsLoading(false);

    await firebase.auth().onAuthStateChanged((usu) => {
      if (usu) {
        setIsAuthenticated(true);
      }
    });
    setIsLoading(true);
  };


  useEffect(() => {
    verify();
  }, []);

  if (!isLoading) {
    return <h1>Cargando</h1>;
  }


  return (
    <div className="App m-5">
      <h1>Quieniela</h1>
      {!isAuthenticated && <LoginPage user={user} setUser={setUser} handleSubmit={handleSubmit} />}
      {isAuthenticated && <Main />}

    </div>

  );
}
