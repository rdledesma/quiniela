import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAIMjuZkQI4CPCAO9u_yDRGaJmyjsEwXTQ',
  authDomain: 'quiniela-52ebc.firebaseapp.com',
  databaseURL: 'https://quiniela-52ebc.firebaseio.com',
  projectId: 'quiniela-52ebc',
  storageBucket: 'quiniela-52ebc.appspot.com',
  messagingSenderId: '1082337462461',
  appId: '1:1082337462461:web:350fbd0fa01ffcb3708fd4',
};

firebase.initializeApp(config);

export default firebase;
